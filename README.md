# README #

This code can run in a chrome extension like "Custom Java Script" to make providing support over the Wordpress.org forums more fun by cleaning up the page.

Running this code on the Wordpress.org forum pages will add 3 options:

**Hide resolved**
Will hide all posts that are either marked as resolved or one of our_team members has the last reply. This is the default mode when loading a page.

**Show all**
You guessed it.

**I prefer to have the last word**
Will show you any thread where the last reply is NOT from our_team, even if the thread is marked as resolved.


## TLDR; ##
1. Install[ csj Chrome browser extension](https://chrome.google.com/webstore/detail/custom-javascript-for-web/poakhlngfciodnhlhhgnaaelnpjljija) (not affiliated, other options should work just as well).
2. Paste [the code](https://bitbucket.org/arne_lap/wordpress-forum-support/src/dd38e7e15fb2c8a41e6cbcdc5cd2e0abdb5221cc/wp-forum-support-assistant.js?at=master&fileviewer=file-view-default) into the cjs plugin while visiting wordpress.org and check "Enable cjs for this host".
3. Fill your support team's Wordpress forum display names in the array **our_team** on top.